import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import createSagaMiddleware from 'redux-saga'

import weatherReducer from './containers/Weather/reducer';

import { weatherSagasWatcher } from './containers/Weather';

const reducers = {
    weather: weatherReducer
};

const sagaMiddleware = createSagaMiddleware();

const enhancers = [
    applyMiddleware(sagaMiddleware)
];

if (process.env.NODE_ENV === 'development' && window) {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension());
    }
}

const store = createStore(
    combineReducers(reducers),
    compose(...enhancers)
);
sagaMiddleware.run(weatherSagasWatcher);

export default store;
