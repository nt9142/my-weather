import { WEATHER_STATES } from '../../utils/constants';
import { ICONS } from '../ForecastIcons';

export const getTemperatureString = (temp) => {
    temp = Math.round(temp);

    if (temp > 0) {
        return `+${temp}`;
    }

    return String(temp);
};

const WEATHER_STATE_TO_ICON_MAP = {
    [WEATHER_STATES.CLEAR]: ICONS.SUN,
    [WEATHER_STATES.CLOUDS]: ICONS.CLOUDS,
    [WEATHER_STATES.RAIN]: ICONS.RAIN,
};

export const getIconFromWeather = (weather) => WEATHER_STATE_TO_ICON_MAP[weather] || ICONS.SUN;
