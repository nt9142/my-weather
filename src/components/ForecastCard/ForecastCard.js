import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import React from 'react';

import { ForecastIcons, ICONS, ICON_SIZES } from '../ForecastIcons';

import { getTemperatureString, getIconFromWeather } from './utils';

const useStyles = makeStyles((theme) => ({
    card: {
        minWidth: 400,
        [theme.breakpoints.down('sm')]: {
            minWidth: 300
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: 500
        },
    },
    cardContent: {
        display: 'flex',
    },
    weatherSummary: {
        flexBasis: '120px',
    },
    weatherGraphic: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexGrow: 1,
    },
    tempContainer: {
        fontWeight: 100,
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    tempExtremum: {
        fontSize: '1em',
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.8em'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '1.5em'
        },
    },
    temp: {
        margin: '-8px 0',

        fontSize: '2.5em',
        [theme.breakpoints.down('sm')]: {
            fontSize: '2em'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '3em'
        },
    }
}));

export function ForecastCard({forecast}) {
    const classes = useStyles();

    const weekDay = moment(forecast.day).format('dddd');
    const date = moment(forecast.day).format('MMMM Do');
    const tempString = getTemperatureString(forecast.temp);
    const minTempString = getTemperatureString(forecast.tempMin);
    const maxTempString = getTemperatureString(forecast.tempMax);
    const weatherIcon = getIconFromWeather(forecast.weather);

    return (
        <Card className={classes.card}>
            <CardContent className={classes.cardContent}>
                <div className={classes.weatherSummary}>
                    <Typography variant="h5" component="h1">
                        {weekDay}
                    </Typography>
                    <Typography color="textSecondary" gutterBottom>
                        {date}
                    </Typography>
                    <Typography variant="h5" component="h2">
                        {forecast.weather}
                    </Typography>
                    <Typography variant="body2" component="p" color="textSecondary">
                        {forecast.description}
                    </Typography>
                </div>
                <div className={classes.weatherGraphic}>
                    <div className={classes.tempContainer}>
                        <Typography className={classes.tempExtremum} color="textSecondary">
                            {maxTempString}
                        </Typography>
                        <div className={classes.temp}>
                            {tempString}
                        </div>
                        <Typography className={classes.tempExtremum} color="textSecondary">
                            {minTempString}
                        </Typography>
                    </div>
                    <ForecastIcons icon={ICONS.CELSIUS} size={ICON_SIZES.M} />
                    <ForecastIcons icon={weatherIcon} />
                </div>
            </CardContent>
        </Card>
    );
}
