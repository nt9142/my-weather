import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

const ENTER_KEY = 13;
const CITY_PLACEHOLDER = 'Select City';


export const CityEditor = (props) => {
    const {city, setCity} = props;
    const [currentCity, setCurrentCity] = useState(city);
    const [isEditorMode, setEditorMode] = useState();

    const handleCityChange = () => {
        if (city !== currentCity) {
            setCity(currentCity);
        }
        setEditorMode(false);
    };
    const handleKeyPress = ({charCode}) => {
        if (charCode === ENTER_KEY) {
            handleCityChange();
        }
    };

    if (!isEditorMode) {
        return (
            <Box>
                <Button onClick={() => setEditorMode(true)}>{city || CITY_PLACEHOLDER}</Button>
            </Box>
        );
    }

    return (
        <Box display="flex">
            <Box>
                <TextField
                    autoFocus
                    label="City"
                    margin="dense"
                    onChange={(e) => setCurrentCity(e.target.value)}
                    onKeyPress={handleKeyPress}
                    placeholder="Berlin"
                    value={currentCity}
                    variant="outlined"
                />
            </Box>
            <Box alignSelf="center" m={1}>
                <Button onClick={handleCityChange}>Choose</Button>
            </Box>
        </Box>
    );
};

CityEditor.propTypes = {
    city: PropTypes.string,
    setCity: PropTypes.func.isRequired
};
