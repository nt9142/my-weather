import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { ICON_SIZES, ICONS, getLetterFromIcon } from './utils';

const useStyles = makeStyles({
    icon: {
        fontFamily: 'WeatherTime',
    }
});
const useIconSizesStyles = makeStyles({
    s: {fontSize: '2em'},
    m: {fontSize: '3em'},
    l: {fontSize: '4em'},
});

export const ForecastIcons = ({size, icon}) => {
    const classes = useStyles();
    const iconSizesClasses = useIconSizesStyles();

    const className = classNames(
        classes.icon, iconSizesClasses[size]
    );

    return (
        <div className={className}>{getLetterFromIcon(icon)}</div>
    )
};

ForecastIcons.propTypes = {
    size: PropTypes.oneOf(Object.values(ICON_SIZES)).isRequired,
    icon: PropTypes.oneOf(Object.values(ICONS)),
};

ForecastIcons.defaultProps = {
    size: ICON_SIZES.L,
};
