export const ICONS = {
    SUN: 'sun',
    CLOUDS: 'clouds',
    RAIN: 'rain',

    CELSIUS: 'celsius',
    FAHRENHEIT: 'fahrenheit',
};

export const ICON_SIZES = {
    S: 's',
    M: 'm',
    L: 'l',
};

export const ICONS_TO_LETTERS_MAP = {
    [ICONS.SUN]: 'A',
    [ICONS.CLOUDS]: 'D',
    [ICONS.RAIN]: 'F',

    [ICONS.CELSIUS]: 'l',
    [ICONS.FAHRENHEIT]: 'm',
};

export const getLetterFromIcon = (icon) => ICONS_TO_LETTERS_MAP[icon];
