import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import React from 'react';
import { makeStyles } from '@material-ui/core';


const useStyles = makeStyles({
    toolbar: {
        height: 60
    }
});

export const Bar = ({children}) => {
    const classes = useStyles();
    return (
        <AppBar color="default">
            <Toolbar className={classes.toolbar} variant="regular">
                {children}
            </Toolbar>
        </AppBar>
    );
};
