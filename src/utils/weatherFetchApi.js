import queryString from 'query-string';

import { WEATHER_UNITS, WEATHER_TO_API_UNITS_MAP } from './constants';

const API_URL = 'http://api.openweathermap.org/data/2.5/';

const getToken = () => (
    // explicit assumption. The application should have a middle server to hide the token there.
    process.env.REACT_APP_WEATHER_TOKEN
);

export const getForecast = (city, weatherUnits = WEATHER_UNITS.CELSIUS) => {
    const query = {
        appId: getToken(),
        q: city,
        units: getApiUnitsFromWeather(weatherUnits)
    };

    return fetch(`${API_URL}/forecast?${queryString.stringify(query)}`)
        .then(data => data.json());
};

const getApiUnitsFromWeather = (weatherUnits) => (
    WEATHER_TO_API_UNITS_MAP[weatherUnits]
);
