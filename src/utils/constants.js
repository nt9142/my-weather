const API_UNITS = {
    METRIC: 'metric',
    IMPERIAL: 'imperial',
};

export const WEATHER_UNITS = {
    CELSIUS: 'CELSIUS',
    FAHRENHEIT: 'FAHRENHEIT'
};

export const WEATHER_TO_API_UNITS_MAP = {
    [WEATHER_UNITS.CELSIUS]: API_UNITS.METRIC,
    [WEATHER_UNITS.FAHRENHEIT]: API_UNITS.IMPERIAL,
};

export const WEATHER_STATES = {
    RAIN: 'Rain',
    CLEAR: 'Clear',
    CLOUDS: 'Clouds',
};

export const LOADING_STATUSES = {
    UNSENT: 'UNSENT',
    LOADING: 'LOADING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
};
