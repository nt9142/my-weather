import get from 'lodash/get';
import moment from 'moment';

// due to a free plan does not include a forecast by days
// there is the function that extracts a 3-hour piece of forecast as daily.
export const extractForecastByDays = ({list}) => {
    const listMap = list.reduce((acc, item) => {
        const dayKey = moment(get(item, 'dt_txt')).format('YYYY-MM-DD');

        acc[dayKey] = {
            day: dayKey,
            weather: get(item, ['weather', 0, 'main']),
            description: get(item, ['weather', 0, 'description']),
            temp: get(item, ['main', 'temp']),
            tempMin: get(item, ['main', 'temp_min']),
            tempMax: get(item, ['main', 'temp_max']),
        };

        return acc;
    }, {});

    return Object.values(listMap);
};
