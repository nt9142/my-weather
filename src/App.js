import React from 'react';

import { Weather } from './containers/Weather';

const App = () => (
    <Weather />
);

export default App;
