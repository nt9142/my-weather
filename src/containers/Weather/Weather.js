import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from './actions';
import * as selectors from './selectors';

import { CityEditor } from '../../components/CityEditor';
import { ForecastCard } from '../../components/ForecastCard';
import { Bar } from '../../components/Bar';


const WeatherComponent = (props) => {
    const {city, forecastByDays, isLoading, isError, setCity} = props;
    return (
        <Container maxWidth="xl">
            <Bar>
                <CityEditor city={city} setCity={setCity} />
            </Bar>
            <Box display="flex" flexDirection="column" pt={10}>
                {isLoading && <Box alignSelf="center"><CircularProgress color="secondary" /></Box>}
                {!isLoading && Boolean(forecastByDays.length) && (
                    <Grid
                        alignItems="center"
                        container
                        direction="column"
                        justify="center"
                        spacing={4}
                    >
                        {forecastByDays.map((dayForecast) => (
                            <Grid key={dayForecast.day} item>
                                <ForecastCard forecast={dayForecast} />
                            </Grid>
                        ))}
                    </Grid>
                )}
                {!isLoading && !isError && !forecastByDays.length && (
                    <Box alignSelf="center">
                        <Typography component="h2" variant="h5">
                            No city has chosen.
                        </Typography>
                    </Box>
                )}
                {isError &&  (
                    <Box alignSelf="center">
                        <Typography component="h2" variant="h5">
                            Error occurred while fetching data. Please try another city.
                        </Typography>
                    </Box>
                )}
            </Box>
        </Container>
    );
};

WeatherComponent.propTypes = {
    city: PropTypes.string,
    forecastByDays: PropTypes.array,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    setCity: PropTypes.func.isRequired,
};

const mapStateToProps = (storeState) => ({
    city: selectors.getCity(storeState),
    forecastByDays: selectors.getForecastByDays(storeState),
    isError: selectors.getIsError(storeState),
    isLoading: selectors.getIsLoading(storeState),
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setCity: actions.setCity
    }, dispatch);
};

export const Weather = connect(mapStateToProps, mapDispatchToProps)(WeatherComponent);
