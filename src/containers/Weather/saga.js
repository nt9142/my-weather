import { call, put, takeLatest } from 'redux-saga/effects'

import { getForecast } from '../../utils/weatherFetchApi';
import { extractForecastByDays } from '../../utils/weatherUtils';
import * as constants from './constants';
import * as actions from './actions';

export function* weatherSagasWatcher() {
    yield takeLatest(constants.SET_CITY, loadForecastSaga)
}

export function* loadForecastSaga({city}) {
    try {
        yield put(actions.loadForecastRequest());

        const data = yield call(getForecast, city);
        const forecastByDays = extractForecastByDays(data);

        yield put(actions.loadForecastSuccess(forecastByDays));
    } catch (e) {
        yield put(actions.loadForecastError());
    }
}

