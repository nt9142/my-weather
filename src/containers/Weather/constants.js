const createConst = name => `WEATHER.${name}`;

export const SET_CITY = createConst('SET_CITY');

export const LOAD_FORECAST_REQUEST = createConst('LOAD_FORECAST_REQUEST');
export const LOAD_FORECAST_SUCCESS = createConst('LOAD_FORECAST_SUCCESS');
export const LOAD_FORECAST_ERROR = createConst('LOAD_FORECAST_ERROR');
