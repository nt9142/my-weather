import produce from 'immer';

import * as CONSTANTS from './constants';
import { LOADING_STATUSES, WEATHER_UNITS } from '../../utils/constants';

const initialState = {
    city: '',
    unit: WEATHER_UNITS.CELSIUS,
    loadingStatus: LOADING_STATUSES.UNSENT,
    forecastByDays: []
};

const weatherReducer = (state = initialState, action) =>
    produce(state, draft => {
    switch (action.type) {
        case CONSTANTS.SET_CITY:
            draft.city = action.city;
            break;
        case CONSTANTS.LOAD_FORECAST_REQUEST:
            draft.loadingStatus = LOADING_STATUSES.LOADING;
            break;
        case CONSTANTS.LOAD_FORECAST_SUCCESS:
            draft.loadingStatus = LOADING_STATUSES.SUCCESS;
            draft.forecastByDays = action.forecastByDays;
            break;
        case CONSTANTS.LOAD_FORECAST_ERROR:
            draft.loadingStatus = LOADING_STATUSES.ERROR;
            draft.forecastByDays = [];
            break;
        // no default
    }
});

export default weatherReducer;
