import * as contstants from './constants';

export const setCity = (city) => ({
    type: contstants.SET_CITY,
    city
});

export const loadForecastRequest = (city) => ({
    type: contstants.LOAD_FORECAST_REQUEST,
    city
});

export const loadForecastSuccess = (forecastByDays) => ({
    type: contstants.LOAD_FORECAST_SUCCESS,
    forecastByDays
});

export const loadForecastError = () => ({
    type: contstants.LOAD_FORECAST_ERROR
});


