import { createSelector } from 'reselect'

import { LOADING_STATUSES } from '../../utils/constants';

const weatherSelector = store => store.weather || {};

export const getCity = createSelector(
    weatherSelector,
    ({city}) => city
);

export const getIsLoading = createSelector(
    weatherSelector,
    ({loadingStatus}) => loadingStatus === LOADING_STATUSES.LOADING
);

export const getIsError = createSelector(
    weatherSelector,
    ({loadingStatus}) => loadingStatus === LOADING_STATUSES.ERROR
);

export const getForecastByDays = createSelector(
    weatherSelector,
    ({forecastByDays}) => forecastByDays
);
