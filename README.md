# MyWeather Service
> based on create-react-app + MaterialUI

## How to start
```bash
# Development mode
$ REACT_APP_WEATHER_TOKEN=<TOKEN> yarn start

# Production
$ yarn build
```
## Assumptions
- API TOKEN holds on the frontend side.  
 Better solution is to use middle server for communicating between frontend and an API service.
